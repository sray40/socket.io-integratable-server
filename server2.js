var http = require('http');//using http library for server module

    
    //instead of serving root domain
	var url = require('url');
	var fs = require('fs');

	//Using Socket.io for realtime 
	var io = require('socket.io');



	
	
    var server = http.createServer(function(request, response){
        var path = url.parse(request.url).pathname;
        console.log(path);

        switch(path){
            case '/':
                response.writeHead(200, {'Content-Type': 'text/html'});
                response.write('log: your server is running');
                response.end();
                break;
            case '/socketServer2.html':
                fs.readFile(__dirname + path, function(error, data){
                    if (error){
                        response.writeHead(404);
                        response.write("Oops this doesn't exist - 404");
                        response.end();
                    }
                    else{
                        response.writeHead(200, {"Content-Type": "text/html"});
                        response.write(data, "utf8");
                        response.end();
                    }
                });
                break;
            default:
                response.writeHead(404);
                response.write("Oops this doesn't exist - 404");
                response.end();
                break;
        }
    });



	server.listen(8001);//kickstarting you server on port 8001 i.e. go to http://localhost:8001 for the log message
	

	var listener = io.listen(server); //opens a listen for socket.io when server is created i.e. therefore the server will be running an listening for an websocket connections that can occur

	/*
	listener.sockets.on('connection', function(socket)
	{

		socket.emit('message',{'message': 'hello samantak'});


	});
	*/

	/*
	//emitting clock data from the socket connection
    listener.sockets.on('connection', function(socket){
    //send data to client
    setInterval(function(){
        socket.emit('date', {'date': new Date()});
    }, 500);
});
*/
	listener.sockets.on('connection', function(socket){
	  //send data to client
	  setInterval(function(){
	    socket.emit('date', {'date': new Date()});
	  }, 1000);

	  //recieve client data
	  socket.on('client_data', function(data){
	    process.stdout.write(data.letter);
	  });
	});


